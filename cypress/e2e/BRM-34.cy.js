describe('BRM-34', () => {
  
  // Run before all the tests  
  beforeEach(() => {
      cy.visit('http://localhost:3000/')
    
      // Use assertions with retries to wait for the correct title
      cy.title().should('not.include', 'blazer.io').then((title) => {
  
        if (!title.includes('blazer.io')) {
          cy.log('Error: Page title is not "blazer.io"')
          
          // cy.fail('Page title is not "blazer.io"')
        }
      })
      cy.wait(4000)
    })
    
    
      it('BRM-34 - Positive', () => {
        
        cy.log("Checking the Header item is visible")
        cy.xpath("//h2[contains(text(),'Recommended Blazers')]").should('be.visible');
        cy.wait(2000);

        // All Products Button
        cy.log('checking All Products')
        cy.xpath("//button[contains(text(),'All Products')]").should('contain','All Products').should('be.visible')
        cy.xpath("//button[contains(text(),'All Products')]").click();
        cy.wait(2000)
        
        // Van Heusen
        cy.log('checking Van Heusen')
        cy.xpath("//button[contains(text(),'Van Heusen')]").should('contain',"Van Heusen").should('be.visible')
        cy.xpath("//button[contains(text(),'Van Heusen')]").click()
        cy.wait(2000)
        
        // Raymond
        cy.log('Raymond')
        cy.xpath("//button[contains(text(),'Raymond')]").should('contain','Raymond').should('be.visible')
        cy.xpath("//button[contains(text(),'Raymond')]").click()
        cy.wait(2000)
        
        // Peter England
        cy.log('checking Peter England')
        cy.xpath("//button[contains(text(),'Peter England')]").should('contain','Peter England').should('be.visible')
        cy.xpath("//button[contains(text(),'Peter England')]").click()
        cy.wait(2000)
        
        // Lois Philippe
        cy.log('Louis Philippe')
        cy.xpath("//button[contains(text(),'Louis Philippe')]").should('contain','Louis Philippe').should('be.visible')
        cy.xpath("//button[contains(text(),'Louis Philippe')]").click()
        cy.wait(2000)



      }
      )

      it('BRM-34 - Positive Sidenav Category', () => {
        
        /**Category
         * */ 
        cy.log('checking for category ')
        cy.xpath("//h2[contains(text(),'Category')]").should('be.visible').click()
        cy.wait(2000)

        // All
        cy.log("Checking for All categories")
        cy.xpath("//body/div[@id='root']/section[1]/div[2]/div[1]/label[1]/span[1]").should('be.visible').click()
        cy.wait(2000)

        // All items 01
        cy.xpath("//body/div[@id='root']/section[1]/div[2]/div[1]/label[1]/span[1]").should('be.visible').s

        // All items 02
        cy.xpath("//body/div[@id='root']/section[2]/section[2]").should('be.visible').scrollIntoView({duration:2000})

        // All items 03
        cy.xpath("//body/div[@id='root']/section[2]/section[3]").should('be.visible').scrollIntoView({duration:2000})

        
        // Tuxedo Jacket
        cy.log("checking for tuxedo jacket")
        cy.xpath("//body/div[@id='root']/section[1]/div[2]/div[1]/label[2]/span[1]").should('be.visible').click()
        cy.wait(2000)

        // Tuxedo Item 01
        cy.xpath("//body/div[@id='root']/section[2]/section[1]").should('be.visible').scrollIntoView({duration:2000})

        // Tuxedo Item 02
        cy.xpath("//body/div[@id='root']/section[2]/section[6]").should('be.visible').scrollIntoView({duration:2000})

        // Tuxedo Item 03
        cy.xpath("//body/div[@id='root']/section[2]/section[9]").should('be.visible').scrollIntoView({duration:2000})

        //Cape Blazer
        cy.log("Checking for Cape Blazer")
        cy.xpath("//body/div[@id='root']/section[1]/div[2]/div[1]/label[3]/span[1]").should('be.visible').click()
        cy.wait(2000)

        // Cape Blazer item 01
        cy.xpath("//body/div[@id='root']/section[2]/section[3]").should('be.visible').scrollIntoView({duration:2000})

        // Cape Blazer Item 02
        cy.xpath("//body/div[@id='root']/section[2]/section[4]").should('be.visible').scrollIntoView({duration:2000})

        // Cape Blazer Item 03
        cy.xpath("//body/div[@id='root']/section[2]/section[4]").should('be.visible').scrollIntoView({duration:2000})

        // Lace Blazer
        cy.log("Checking for Lace Balzer")
        cy.xpath("//body/div[@id='root']/section[1]/div[2]/div[1]/label[4]/span[1]").should('be.visible').click()
        cy.wait(2000)

        // Lace Blazer Item 01
        cy.xpath("//body/div[@id='root']/section[2]/section[1]").should('be.visible').scrollIntoView({duration:2000})

        // Lace Blazer Item 02
        cy.xpath("//body/div[@id='root']/section[2]/section[2]").should('be.visible').scrollIntoView({duration:2000})

        // Lace Blazer Item 03
        cy.xpath("//body/div[@id='root']/section[2]/section[3]").should('be.visible').scrollIntoView({duration:2000})

        // Linen blazer
        cy.log("checking for Linen Blazer")
        cy.xpath("//body/div[@id='root']/section[1]/div[2]/div[1]/label[5]/span[1]").should('be.visible').click()
        cy.wait(2000)

        // LinenBlazer Item 01
        cy.xpath("//body/div[@id='root']/section[2]/section[1]").should('be.visible').scrollIntoView({duration:2000})

        // Linen Item 02
        cy.xpath("//body/div[@id='root']/section[2]/section[2]").should('be.visible').scrollIntoView({duration:2000})

        // Linen Item 03
        cy.xpath("//body/div[@id='root']/section[2]/section[7]").should('be.visible').scrollIntoView({duration:2000})
        
      }
      )

      it('BRM-34 - Positive Sidenav Price', () => {
        
        // All
        cy.log("Checking for All Price Ranges")
        cy.xpath("//body/div[@id='root']/section[1]/div[3]/label[1]/span[1]").should('be.visible').click()
        cy.wait(2000)

        // All Item 01
        cy.xpath("//body/div[@id='root']/section[2]/section[5]").should('be.visible').scrollIntoView({duration:2000})
        // All Item 02
        cy.xpath("//body/div[@id='root']/section[2]/section[8]").should('be.visible').scrollIntoView({duration:2000})
        // All Item 03
        cy.xpath("//body/div[@id='root']/section[2]/section[11]").should('be.visible').scrollIntoView({duration:2000})

        // Rs 1000-2000
        cy.log("Checking for Rs 1000-2000 range")
        cy.xpath("//body/div[@id='root']/section[1]/div[3]/label[2]/span[1]").should('be.visible').click()
        cy.wait(2000)

        // Rs 2000-3000
        cy.log("Checking for Rs 2000 - 3000 Range")
        cy.xpath("//body/div[@id='root']/section[1]/div[3]/label[3]/span[1]").should('be.visible').click()
        cy.wait(2000)

        // Rs 3000-4000
        cy.log('Checking for Rs 3000-4000 Range')
        cy.xpath("//body/div[@id='root']/section[1]/div[3]/label[4]/span[1]").should('be.visible').click()
        cy.wait(2000)

        // Over Rs 4000
        cy.log("Checking for Over Rs 4000 Range")
        cy.xpath("//body/div[@id='root']/section[1]/div[3]/label[5]/span[1]").should('be.visible').click()
        cy.wait(2000)
        
      }
      )

      it('BRM-34 - Positive Sidenav Colors', () => {
        
        // All
        cy.log("Checking for all colour ranges")
        cy.xpath("//body/div[@id='root']/section[1]/div[3]/label[5]/span[1]").click({ force: true })
        cy.wait(2000)

        // All items black 01
        cy.log("Checking for All items Black")
        cy.xpath("//body/div[@id='root']/section[2]/section[1]").should('be.visible').should('contain','black').scrollIntoView({duration:2000})

        // All items blue
        cy.xpath("//body/div[@id='root']/section[2]/section[5]").should('be.visible').scrollIntoView({duration:2000})

        // All items red
        cy.xpath("//body/div[@id='root']/section[2]/section[8]").should('be.visible').scrollIntoView({duration:2000})

        // Black
        cy.log("checking for black color")
        cy.xpath("//body/div[@id='root']/section[1]/div[4]/label[2]/span[1]").click({ force: true })
        cy.wait(2000)

        // Black Item 01
        cy.xpath("//body/div[@id='root']/section[2]/section[5]").should('be.visible').scrollIntoView({duration:2000})

        // Black Item 02
        cy.xpath("//body/div[@id='root']/section[2]/section[8]").should('be.visible').scrollIntoView({duration:2000})

        // Black Item 03
        cy.xpath("//body/div[@id='root']/section[2]/section[12]").should('be.visible').scrollIntoView({duration:2000})

        // Blue
        cy.log("Checking for Blue color")
        cy.xpath("//body/div[@id='root']/section[1]/div[4]/label[3]/span[1]").click({ force: true })
        cy.wait(2000)
       
        // Blue Item 01
        cy.xpath("//body/div[@id='root']/section[2]/section[1]").should('be.visible').scrollIntoView({duration:2000})

        // Blue Item 02
        cy.xpath("//body/div[@id='root']/section[2]/section[3]").should('be.visible').scrollIntoView({duration:2000})

        // Blue Item 03
        cy.xpath("//body/div[@id='root']/section[2]/section[4]").should('be.visible').scrollIntoView({duration:2000})

        // Red
        cy.log("Checking for red color")
        cy.xpath("//body/div[@id='root']/section[1]/div[4]/label[4]/span[1]").click({ force: true })
        cy.wait(2000)

         // Red Item 01
         cy.xpath("//body/div[@id='root']/section[2]/section[1]").should('be.visible').scrollIntoView({duration:2000})

        // Red Item 02
        cy.xpath("//body/div[@id='root']/section[2]/section[4]").should('be.visible').scrollIntoView({duration:2000})

        // Red Item 03
        cy.xpath("//body/div[@id='root']/section[2]/section[6]").should('be.visible').scrollIntoView({duration:2000})
        
      }
      )
    
      it('BRM-34 Negative', () => {

      }
    
    
      )
    })
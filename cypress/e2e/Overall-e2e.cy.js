describe('Overall Process Final Release Testing', () => {

    before(() => {
        cy.visit('http://localhost:3000/home')
    })



    it('Overall Process', () => {

        // Login Process

        cy.log("Navigate to the login")
        cy.xpath("//header/div[1]/div[1]/div[3]/span[2]/a[1]/i[1]").should('be.visible').click()

        cy.log("Checking the avaialbility of the header")
        cy.xpath("//h2[contains(text(),'Login')]").should('be.visible')
        cy.wait(200)

        cy.log("Checking the Blazer.io")
        cy.xpath("//div[contains(text(),'blazer.io')]").should("be.visible")
        cy.wait(200)

        cy.log("Checking the Login Header")
        cy.xpath("//h1[contains(text(),'Login')]").should("be.visible")
        cy.wait(200)

        cy.log("Navigation to register")
        cy.xpath("//button[contains(text(),'Sign up')]").click()
        cy.wait(200)

        // Register Process

        cy.log("checking the username")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/input[1]").should("be.visible").click().type('Dimuth C Bandara')
        cy.wait(200)

        cy.log("Enter Username")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/input[1]").should("be.visible").click().type("password")
        cy.wait(200)

        cy.log("Confirm Password")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/input[1]").should("be.visible").click().type("password")
        cy.wait(200)

        cy.log("Register User")
        cy.xpath("//button[contains(text(),'Sign up')]").should("be.visible").click()
        cy.wait(200)

        // Login Functions
        cy.log("Username")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/input[1]").should('be.visible').click().type("Dimuth C Bandara")
        cy.wait(200)

        cy.log("Password")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/input[1]").should('be.visible').click().type("password")
        cy.wait(200)

        cy.log('Login')
        cy.xpath("//button[contains(text(),'Sign in')]").click()
        cy.wait(200)

        // Home Screen Functionalities
        cy.log("Checking the availabity of moto")
        cy.xpath("//h5[contains(text(),'Easy way to make an order')]").should('be.visible')
        cy.wait(200)

        cy.log("checking the visibility of Topic")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/h1[1]").should('be.visible').contains("Want to be SMART ?")
        cy.wait(200)

        cy.log("Checking for details")
        cy.xpath("//p[contains(text(),'Lorem ipsum dolor sit amet, consectetur adipisicin')]").should("be.visible")
        cy.wait(200)

        cy.log("Button -> Orde Now")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]")
            .should('be.visible')
            .trigger('mouseover')
        //.should('have.css', 'property-name', 'expected-value'); // Check the expected CSS property value during hover
        cy.wait(2000)

        cy.log("Checking the button -> All Details")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[2]").should("be.visible").trigger('mouseover')
        cy.wait(200)

        cy.log("No shopping cart display")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[2]/p[1]").should('be.visible')
        cy.wait(200)

        cy.log("Checking the availability of details")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[2]/p[2]").should("be.visible")
        cy.wait(200)

        cy.log("Availability of the image")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[2]/div[1]/img[1]").should("be.visible")
        cy.wait(200)

        cy.log("category 01 - Van Heusan")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/div[1]").trigger('mouseover').should('be.visible')
        cy.wait(200)

        cy.log(" Category 02 - Raymond")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[2]/div[1]").should('be.visible').trigger('mouseover')
        cy.wait(200)

        cy.log("Category 03 - Peter England")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]").should("be.visible").trigger("mouseover")
        cy.wait(200)

        cy.log("Catrgory 04 - Louis Philppe")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[4]/div[1]").should("be.visible").trigger("mouseover")
        cy.wait(200)

        cy.log("Serve Option")
        cy.xpath("//h5[contains(text(),'What we serve')]").should('be.visible')
        cy.wait(200)

        cy.log("Topic Header")
        cy.xpath("//h2[contains(text(),'Just sit back at home')]").should("be.visible")
        cy.wait(200)

        cy.log("Paragrph")
        cy.get("div.w-100 section:nth-child(3) div.container div.row div.text-center.col-lg-12:nth-child(1) > p.mb-1.mt-4.feature__text:nth-child(4)").should('be.visible')
        cy.wait(200)

        cy.log('Delivery Image')
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[3]/div[1]/div[1]/div[2]/div[1]/img[1]").should('be.visible')
        cy.wait(200)

        cy.log("Delivery Topic")
        cy.xpath("//h5[contains(text(),'Quick Delivery')]").should("be.visible")
        cy.wait(200)

        cy.log("Delivery Description")
        cy.xpath("//body[1]/div[1]/div[1]/div[1]/div[1]/section[3]/div[1]/div[1]/div[2]/div[1]/p[1]").should('be.visible')
        cy.wait(200)

        cy.log("Super Fashion Logo")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[3]/div[1]/div[1]/div[3]/div[1]/img[1]").should('be.visible')
        cy.wait(200)

        cy.log("Super Fashion Topic")
        cy.xpath("//h5[contains(text(),'Super Fashion')]").should('be.visible')
        cy.wait(200)

        cy.log("Super Fashion Description")
        cy.xpath("//body[1]/div[1]/div[1]/div[1]/div[1]/section[3]/div[1]/div[1]/div[3]/div[1]/p[1]").should('be.visible')
        cy.wait(200)

        cy.log("Easy Rental image")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[3]/div[1]/div[1]/div[4]/div[1]/img[1]").should('be.visible')
        cy.wait(200)

        cy.log("Easy Rental Topic")
        cy.xpath("//h5[contains(text(),'Easy Rental')]").should('be.visible')
        cy.wait(200)

        cy.log("Easy Rental Paragraph")
        cy.xpath("//body[1]/div[1]/div[1]/div[1]/div[1]/section[3]/div[1]/div[1]/div[4]/div[1]/p[1]").should('be.visible')
        cy.wait(200)

        cy.log('Poupalr blazer - Topic')
        cy.xpath("//h2[contains(text(),'Popular blazers')]").should('be.visible')
        cy.wait(200)

        cy.log("All Buttons Clicking")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[2]/div[1]/button[1]").should('be.visible').click()
        cy.wait(200)

        cy.log("All Item 01")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[3]/div[1]").should('be.visible')
        cy.wait(200)

        cy.log('Item 01- Image')
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[3]/div[1]/div[1]/img[1]").should('be.visible')
        cy.wait(200)

        cy.log('Item 01 - Topic')
        cy.xpath("//a[contains(text(),'Raymond Black')]").should('be.visible')
        cy.wait(200)

        cy.log('All Item 01 - Price')
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/span[1]").should('be.visible')
        cy.wait(200)

        cy.log('All Item 01 - Button')
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/button[1]").should('be.visible')
        cy.wait(200)

        cy.log("Raymonds Button Click")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[2]/div[1]/button[2]").click()
        cy.wait(200)

        cy.log("Raymond Item 01")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[3]/div[1]").should('be.visible')
        cy.wait(200)

        cy.log("Van Heusaan Button click")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[2]/div[1]/button[3]").click()
        cy.wait(200)

        cy.log("Van Hesaun Item 01")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[3]/div[1]").should('be.visible')
        cy.wait(200)

        cy.log("Peter England Button click")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[2]/div[1]/button[4]").click()
        cy.wait(200)

        cy.log("Peter England Item 01")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[4]/div[1]/div[1]/div[3]/div[1]").should('be.visible')
        cy.wait(200)

        cy.log("Why Balzer Rental Image")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[5]/div[1]/div[1]/div[1]/img[1]").should('be.visible')
        cy.wait(200)

        cy.log("Why Blazer Rental Topic")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[5]/div[1]/div[1]/div[2]/div[1]/h2[1]").should('be.visible')
        cy.wait(200)

        cy.log("Ehy Blazer Rental Para")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[5]/div[1]/div[1]/div[2]/div[1]/p[1]").should('be.visible')
        cy.wait(200)

        cy.log("Popular Van Heusan Topic")
        cy.xpath("//h2[contains(text(),'Popular VanHeusen')]").should('be.visible')
        cy.wait(200)

        cy.log('Popular Van Heusan Item 01')
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[6]/div[1]/div[1]/div[2]/div[1]").should('be.visible')
        cy.wait(200)

        cy.log("Testimonial Topic")
        cy.xpath("//h5[contains(text(),'Testimonial')]").should('be.visible')
        cy.wait(200)

        cy.log("Testimonial Sub Topic")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[7]/div[1]/div[1]/div[1]/div[1]/h2[1]").should('be.visible')
        cy.wait(200)

        cy.log("Testimonial Image")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[7]/div[1]/div[1]/div[2]/img[1]").should('be.visible')
        cy.wait(200)

        cy.log("Testimonial Paragraph")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[7]/div[1]/div[1]/div[2]/img[1]").should('be.visible')
        cy.wait(200)

        // Moving into the blzer section
        cy.log('moving into blazer Section')
        cy.xpath("//a[contains(text(),'Blazers')]").click()
        cy.wait(500)

        cy.log("Checking for the header")
      cy.xpath("//h2[contains(text(),'All Details')]").should("be.visible")
      cy.wait(200)

      cy.log("Checking Item 01")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]").should("be.visible")
        cy.wait(200)

        cy.log("Checking Item 02")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[4]/div[1]").should("be.visible")
        cy.wait(200)

        cy.log("Checking Item 04")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[5]/div[1]").should("be.visible")
        cy.wait(200)
       
        cy.log("Checking Item 05")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[7]/div[1]").should("be.visible")
        cy.wait(200)

        cy.log("Checking Item 06")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[11]/div[1]").should("be.visible")
        cy.wait(200)

        cy.log("Item 01 Add to cart")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/button[1]").click()
        cy.wait(200)

        cy.log('Cart Item Check')
        cy.xpath("//header/div[1]/div[1]/div[3]/span[1]/i[1]").click()
        cy.wait(200)

        cy.log('View Cart Item')
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/ul[1]/div[2]/li[1]/div[1]").should('be.visible')
        cy.wait(200)

        cy.log("check on plus button")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/ul[1]/div[2]/li[1]/div[1]/div[1]/div[1]/div[1]/span[1]/i[1]").click()
        cy.wait(200)

        cy.log("Check negative button")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/ul[1]/div[2]/li[1]/div[1]/div[1]/div[1]/div[1]/span[3]/i[1]").click()
        cy.wait(200)

        cy.log("Proceed to checkout")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/ul[1]/div[3]/button[1]").click()
        cy.wait(200)

        cy.log("Header Confirmation Checkout")
        cy.xpath("//h2[contains(text(),'Checkout')]").should('be.visible')
        cy.wait(200)

        cy.log("Moving into Cart Page")
        cy.xpath("//a[contains(text(),'Cart')]").click()
        cy.wait(200)

        cy.log("Header of cart")
        cy.xpath("//h2[contains(text(),'Your Cart')]").should('be.visible')
        cy.wait(200)

        cy.log('Item Image')
        cy.xpath("//tbody/tr[1]/td[1]/img[1]").should('be.visible')
        cy.wait(200)

        cy.log("Cart Item Name")
        cy.get('div.w-100 div.container div.row div.col-lg-12 table.table.table-bordered tbody:nth-child(2) tr:nth-child(1) > td.text-center:nth-child(2)').should('be.visible')
        cy.wait(200)

        cy.log('Cart Price')
        cy.get('div.w-100 div.container div.row div.col-lg-12 table.table.table-bordered tbody:nth-child(2) tr:nth-child(1) > td.text-center:nth-child(3)').should('be.visible')
        cy.wait(200)

        cy.log('Number of pieces')
        cy.get("div.w-100 div.container div.row div.col-lg-12 table.table.table-bordered tbody:nth-child(2) tr:nth-child(1) > td.text-center:nth-child(4)").should('be.visible')
        cy.wait(200)

        cy.log('Remove From Cart')
        cy.xpath('//tbody/tr[1]/td[5]').should('be.visible')
        cy.wait(200)

        cy.log('Perform Checkout')
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/div[1]/div[1]/button[2]").click()
        cy.wait(200)

        cy.log('chaeckot header')
        cy.xpath("//h2[contains(text(),'Checkout')]").should('be.visible')
        cy.wait(200)

        cy.log("subtotal availability")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[2]/div[1]/h6[1]").should('be.visible')
        cy.wait(200)

        cy.log("shipping Charges Avaialability")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[2]/div[1]/h6[2]").should('be.visible')
        cy.wait(200)

        cy.log("Total Value")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[2]/div[1]/div[1]/h5[1]").should('be.visible')
        cy.wait(200)

        cy.log("Enter Name")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/form[1]/div[1]/input[1]").click().type("Dimuth")
        cy.wait(200)

        cy.log('Email')
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/form[1]/div[2]/input[1]").click().type("dimuthcbandara97@gmail.com")
        cy.wait(200)

        cy.log("Phone Number")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/form[1]/div[3]/input[1]").click().type("0767212783")
        cy.wait(200)

        cy.log("Country")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/form[1]/div[4]/input[1]").click().type("Sri Lanka")
        cy.wait(200)

        cy.log('City')
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/form[1]/div[5]/input[1]").click().type("Kurunegala")
        cy.wait(200)

        cy.log("Country Code")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/form[1]/div[6]/input[1]").click().type("+94")
        cy.wait(200)

        cy.log("Payment Button")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/form[1]/div[6]/input[1]").click()
        cy.wait(2000)

        cy.log("This is the ending of the testing")

    }
    )



})
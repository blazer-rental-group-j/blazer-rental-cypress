describe('BRM-29', () => {
  
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    
      // Use assertions with retries to wait for the correct title
      cy.title().should('not.include', 'blazer.io').then((title) => {
  
        if (!title.includes('blazer.io')) {
          cy.log('Error: Page title is not "blazer.io"')
          
          // cy.fail('Page title is not "blazer.io"')
        }
      })
      cy.wait(4000)
    })
    
    
      it('BRM-29 - Positive', () => {
        
        cy.log("Checking the Header item is visible")
        cy.xpath("//h2[contains(text(),'Recommended Blazers')]").should('be.visible');
        cy.wait(2000);

        // All Products Button
        cy.log('checking All Products')
        cy.xpath("//button[contains(text(),'All Products')]").should('contain','All Products').should('be.visible')
        cy.wait(2000)
        // Van Heusen
        cy.log('checking Van Heusen')
        cy.xpath("//button[contains(text(),'Van Heusen')]").should('contain',"Van Heusen").should('be.visible')
        cy.wait(2000)
        // Raymond
        cy.log('Raymond')
        cy.xpath("//button[contains(text(),'Raymond')]").should('contain','Raymond').should('be.visible')
        cy.wait(2000)
        // Peter England
        cy.log('checking Peter England')
        cy.xpath("//button[contains(text(),'Peter England')]").should('contain','Peter England').should('be.visible')
        cy.wait(2000)
        // Lois Philippe
        cy.log('Louis Philippe')
        cy.xpath("//button[contains(text(),'Louis Philippe')]").should('contain','Louis Philippe').should('be.visible')
        cy.wait(2000)

        cy.log("Checking the avialbilty of card items")
        //cart item
        cy.xpath("//body/div[@id='root']/section[2]/section[1]").should('be.visible').click()
        cy.wait(2000)

        // item name
        cy.log('checking visibility of item name')
        cy.xpath("//body/div[@id='root']/section[2]/section[1]/div[1]/h3[1]").should('be.visible')
        cy.wait(2000)
        
        // rating
        cy.log('checking visibility of rating')
        cy.xpath("//body/div[@id='root']/section[2]/section[1]/div[1]/section[1]").should('be.visible')
        cy.wait (2000)
        
        // price
        cy.log('checking visibility of price')
        cy.xpath("//body/div[@id='root']/section[2]/section[1]/div[1]/section[2]/div[1]/div[1]").should('be.visible')
        cy.wait(2000)
        
        // availability
        cy.log('checking visibility of availability')
        cy.xpath("//body/div[@id='root']/section[2]/section[1]/div[1]/section[2]/div[1]/div[1]").should('be.visible')
        cy.wait (2000)
        
        // image
        cy.log('checking visibility of ard image')
        cy.xpath("//body/div[@id='root']/section[2]/section[1]/img[1]").should('be.visible')
        cy.wait(2000)

        cy.wait(2000)

        cy.log("Navigation into default cards")
        cy.xpath("//body/div[@id='root']/section[2]/section[2]").scrollIntoView({duration:2000})

        cy.log("Navigation into default cards")
        cy.xpath("//body/div[@id='root']/section[2]/section[4]").scrollIntoView({duration:2000})

        cy.log("Navigation into default cards")
        cy.xpath("//body/div[@id='root']/section[2]/section[8]").scrollIntoView({duration:2000})

        cy.log("Navigation into default cards")
        cy.xpath("//body/div[@id='root']/section[2]/section[12]").scrollIntoView({duration:2000})

      }
      )
    
      it('BRM-29 Negative', () => {
      }
    
    
      )
    })
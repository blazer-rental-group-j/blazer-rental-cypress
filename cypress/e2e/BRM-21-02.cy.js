describe('BRM-21-Signup', () => {
  
    beforeEach(()=>{
      cy.visit('http://127.0.0.1:5173/')
      cy.title().should('eq','blazer.io')
  })
  
    it('BRM-21-Signup Positive', () => {

      cy.log('Checking the availability of the header')
      cy.get('body:nth-child(2) div:nth-child(1) div.app-signup > div.logo').should('be.visible');
      cy.wait(500)

      cy.log('Checking the availability of the Sign Up text')
      cy.get('body:nth-child(2) div:nth-child(1) div.app-signup div.login-form > h1:nth-child(1)').should('be.visible')
      cy.wait(500)

      cy.log('checking the username')
      cy.get('div:nth-child(1) div.app-signup div.login-form div.text-input:nth-child(2) > input.form-control').type('Dimuth C Bandara')
      cy.wait(1000)

      cy.log('checking the password')
      cy.get('div:nth-child(1) div.app-signup div.login-form div.text-input:nth-child(3) > input.form-control').type('password')
      cy.wait(1000)

      cy.log('checking the click option of the Sign In')
      cy.get('div:nth-child(1) div.app-signup div.login-form div.d-grid.gap-1:nth-child(7) > button.btn.btn-outline-dark').click()
      cy.wait(1000)
      
      cy.log('checking the confirm password')
      cy.get('div:nth-child(1) div.app-signup div.login-form div.text-input:nth-child(4) > input.form-control').type('password')
      
      cy.wait(500)
      
      cy.log('check the clicking action of the button')
      cy.get('div:nth-child(1) div.app-signup div.login-form div.d-grid.gap-1:nth-child(5) > button.btn.btn-dark.mt-3').click()
    }
    )
  
    it('BRM-21-Signup Negative', () => {
        
      cy.log('checking for invalid username')
      cy.get('div:nth-child(1) div.app-signup div.login-form div.text-input:nth-child(2) > input.form-control').type('Dimuth,;l C Bandara')
        cy.wait(1000)
        
        cy.log('checking for invalid password')
        cy.get('div:nth-child(1) div.app-signup div.login-form div.text-input:nth-child(3) > input.form-control').type('password')
        cy.wait(1000)
        
        cy.log('checking for invalid confirm password')
        cy.get('div:nth-child(1) div.app-signup div.login-form div.text-input:nth-child(4) > input.form-control').type('passclxword')
        cy.wait(500)
        
        cy.get('div:nth-child(1) div.app-signup div.login-form div.d-grid.gap-1:nth-child(5) > button.btn.btn-dark.mt-3').click()
    }
  
  
    )
  })
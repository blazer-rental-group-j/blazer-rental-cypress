describe('BRM-39 Blazer Testing', () => {
  
    beforeEach(()=>{
      cy.visit('http://localhost:3000/blazers')
  })
  
    it('BRM-39 Postive Testing - Click', () => {
      
      cy.log("Checking for the header")
      cy.xpath("//h2[contains(text(),'All Details')]").should("be.visible")
      cy.wait(200)


    }
    )

    it('BRM-39 Negative Testing', () => {


    }
    )

    // Section 01 - Your Cart
    it('BRM-39 Postive Testing - Your Cart', () => {
      
        cy.log("Checking Item 01")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]").should("be.visible")
        cy.wait(200)

        cy.log("Checking Item 02")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[4]/div[1]").should("be.visible")
        cy.wait(200)

        cy.log("Checking Item 04")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[5]/div[1]").should("be.visible")
        cy.wait(200)
       
        cy.log("Checking Item 05")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[7]/div[1]").should("be.visible")
        cy.wait(200)

        cy.log("Checking Item 06")
        cy.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[11]/div[1]").should("be.visible")
        cy.wait(200)

      }
      )
  
      it('BRM-21 Negative Testing', () => {
  
  
      }
      )

    // Section 02 - Cart Details
    it('BRM-39 Postive Testing - Cart Details', () => {

      }
      )
  
      it('BRM-39 Negative Testing', () => {
  
  
      }
      )

      // Section 03 - Footer
      it('BRM-39 Postive Testing - Footer', () => {

      }
      )
  
      it('BRM-39 Negative Testing', () => {
  
  
      }
      )

     
  })
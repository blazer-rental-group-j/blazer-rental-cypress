describe('BRM-21 Login Testing', () => {
  
    beforeEach(()=>{
      cy.visit('http://127.0.0.1:5173/')
  })
  
    it('BRM-21 Postive Testing', () => {
      cy.get('div:nth-child(1) div.app-login div.login-form div.text-input:nth-child(2) > input.form-control').type("admin")
      cy.get('div:nth-child(1) div.app-login div.login-form div.text-input:nth-child(3) > input.form-control').type('password')
      cy.get('div:nth-child(1) div.app-login div.login-form div.d-grid.gap-1:nth-child(4) > button.btn.btn-dark.mt-3').click()
      cy.wait(2000)
      cy.get('div:nth-child(1) div.app-login div.login-form div.d-grid.gap-1:nth-child(6) > button.btn.btn-outline-dark').click()
      cy.wait(2000);
    }
    )
  
    it('BRM-21 Negative Testing', () => {
      cy.get('div:nth-child(1) div.app-login div.login-form div.text-input:nth-child(2) > input.form-control').type("HelloWolrd")
      cy.get('div:nth-child(1) div.app-login div.login-form div.text-input:nth-child(3) > input.form-control').type('sflfd,')
      cy.get('div:nth-child(1) div.app-login div.login-form div.d-grid.gap-1:nth-child(4) > button.btn.btn-dark.mt-3').click()
      cy.wait(2000)
      cy.get('div:nth-child(1) div.app-login div.login-form div.d-grid.gap-1:nth-child(6) > button.btn.btn-outline-dark').click()
      cy.wait(2000);
    }
  
  
    )
  })